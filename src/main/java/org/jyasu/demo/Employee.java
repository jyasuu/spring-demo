package org.jyasu.demo;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;

@Document(indexName = "employee")
@Data
public class Employee {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String department;
}