package org.jyasu.demo;

public class ApiException extends Exception {

    public ApiException(int code){
        this.code = code;
    }

    public int code ;

    public int getCode() {
        return code;
    }

}
