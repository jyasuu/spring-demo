package org.jyasu.demo;

import org.springframework.beans.factory.annotation.Value;

public interface TestDTO {
    @Value("#{target.test_version}")
    public String getTestVersion();
}