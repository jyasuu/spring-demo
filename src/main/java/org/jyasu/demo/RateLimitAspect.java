package org.jyasu.demo;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RateLimitAspect {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Around("(@annotation(org.jyasu.demo.RateLimit))")
    public Object logging(ProceedingJoinPoint joinPoint) throws Throwable {

        logger.info("RateLimitAspect...");

        joinPoint.proceed();
        throw new ApiException(2000);
    }

}