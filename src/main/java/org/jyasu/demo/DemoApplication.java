package org.jyasu.demo;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableScheduling
@RestController
@RequestMapping("/api")
@Slf4j
public class DemoApplication {

	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	static final String topicExchangeName = "spring-boot-exchange";

	static final String queueName = "spring-boot";

	@Bean
	Queue queue() {
		return new Queue(queueName, false);
	}

	@Bean
	TopicExchange exchange() {
		return new TopicExchange(topicExchangeName);
	}

	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with("foo.bar.#");
	}
	
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
	RedisTemplate<String,String> redisTemplate;

	
	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {
		CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
		loggingFilter.setIncludeClientInfo(true);
		loggingFilter.setIncludeQueryString(true);
		loggingFilter.setIncludePayload(true);
		loggingFilter.setMaxPayloadLength(64000);
		return loggingFilter;
	}
    

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@RateLimit
    @GetMapping("/")
    public String sayHello() {
        return "Hello World";
    }    

    @GetMapping("/error")
    public String getError() throws ApiException {
		log.info("getError!");
        throw new ApiException(1000);
    }

    @PostMapping("/post")
    public String create(@RequestBody String body) throws ApiException {
		log.info("getError!");
        throw new ApiException(3000);
    }

    @GetMapping("/solr1")
    public String solr1() throws Exception {
		
		SolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/leads").build(); 
		SolrInputDocument document = new SolrInputDocument();
        document.addField("id", "1");
        document.addField("name", "John Doe" );
        document.addField("location", "San Francisco, CA" );
        document.addField("serviceType", "Interior Design" );
        document.addField("status", "New");
        document.addField("notes",  "Looking for modern style");

        solrClient.add(document);
        solrClient.commit();

		return "";
    }

    @GetMapping("/solr2")
    public List<Lead> solr2() throws Exception {
		
		SolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/leads").build(); // new Http2SolrClient.Builder("http://localhost:8983/solr/leads").build();
		String searchTerm = "modern", location = "San Francisco, CA",  status = "New";
		
		SolrQuery query = new SolrQuery();
        query.setQuery("notes:" + searchTerm + " OR name:" + searchTerm);
        if (location != null) {
            query.addFilterQuery("location:" + location);
        }
        if (status != null) {
            query.addFilterQuery("status:" + status);
        }
        query.setStart(0); // Pagination
        query.setRows(10); // Pagination

        QueryResponse response = solrClient.query(query);
        SolrDocumentList documents = response.getResults();

        List<Lead> leads = new ArrayList<>();
        for (SolrDocument doc : documents) {
            Lead lead = new Lead();
            lead.setId((String) doc.getFieldValue("id"));
            lead.setName((ArrayList) doc.getFieldValue("name"));
            lead.setLocation((ArrayList) doc.getFieldValue("location"));
            lead.setServiceType((ArrayList) doc.getFieldValue("serviceType"));
            lead.setStatus((ArrayList) doc.getFieldValue("status"));
            lead.setNotes((ArrayList) doc.getFieldValue("notes"));
            leads.add(lead);
        }

        return leads;

    }
	
    @Autowired
    private EmployeeRepository employeeRepository;

    @PostMapping("/employees")
    public Employee create(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }
    @GetMapping("/employees/{id}")
    public Optional<Employee> findById(@PathVariable String id) {
        return employeeRepository.findById(id);
    }

    @GetMapping("/employees")
    public Iterable<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @PutMapping("/employees/{id}")
    public Employee update(@PathVariable String id, @RequestBody Employee employee) {
        employee.setId(id);
        return employeeRepository.save(employee);
    }

    @DeleteMapping("/employees/{id}")
    public void delete(@PathVariable String id) {
        employeeRepository.deleteById(id);
    }

	@Scheduled(cron = "* * * * * *")
	public void job(){
		log.info("Job run!");

		List<Map<String, Object>> rows = jdbcTemplate.queryForList("select version()");
		for(Map<String, Object> row : rows){
			log.info("row {}",row);
		}


        String message = "Hello World!";
        rabbitTemplate.convertAndSend("spring-boot", message);
        rabbitTemplate.convertAndSend("spring-boot-exchange", "foo.bar.baz", message);

		redisTemplate.opsForList().leftPush("A", message);


	}

	@Autowired
	CustomerRepository repository;

	@Scheduled(cron = "*/5 * * * * *")
	public void job2(){
		log.info("Job2 {}!",redisTemplate.opsForList().rightPop("A"));
		log.info("Job2 {}!",redisTemplate.opsForList().size("A"));

		repository.getVersion(TestDTO.class).forEach(dto -> log.info("Job2 ======= {}!",dto.getTestVersion()));

		
		// save a few customers
		repository.save(new Customer("Jack", "Bauer"));
		repository.save(new Customer("Chloe", "O'Brian"));
		repository.save(new Customer("Kim", "Bauer"));
		repository.save(new Customer("David", "Palmer"));
		repository.save(new Customer("Michelle", "Dessler"));

		// fetch all customers
		log.info("Customers found with findAll():");
		log.info("-------------------------------");
		repository.findAll().forEach(customer -> {
			log.info(customer.toString());
		});
		log.info("");

		// fetch an individual customer by ID
		Customer customer = repository.findById(1L);
		log.info("Customer found with findById(1L):");
		log.info("--------------------------------");
		log.info(customer.toString());
		log.info("");

		// fetch customers by last name
		log.info("Customer found with findByLastName('Bauer'):");
		log.info("--------------------------------------------");
		repository.findByLastName("Bauer").forEach(bauer -> {
			log.info(bauer.toString());
		});
		log.info("");
		


	}

	@Component
	@RabbitListener(queues = "spring-boot")
	public class Receiver {

		@RabbitHandler
		public void receive(String in) {
			log.info(" [x] Received '" + in + "'");
		}
	}

}
