package org.jyasu.demo;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Lead {
    private String id;
    private ArrayList<Object> name;
    private ArrayList<Object> location;
    private ArrayList<Object> serviceType;
    private ArrayList<Object> status;
    private ArrayList<Object> notes;
    
}
