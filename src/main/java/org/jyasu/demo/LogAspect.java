package org.jyasu.demo;

import java.io.UnsupportedEncodingException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LogAspect {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ObjectMapper mapper;

    @Pointcut("bean(*Controller)")
    public void logPointCut() { }

    @Around("(@annotation(org.springframework.web.bind.annotation.GetMapping))")
    public Object logging(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        int lastPointIndex = joinPoint.getSignature().getDeclaringTypeName().lastIndexOf(".") + 1; 
        String class_method = joinPoint.getSignature().getDeclaringTypeName().substring(lastPointIndex) + "." +
                joinPoint.getSignature().getName() + "()";

        Object responseBody = joinPoint.proceed();
        logger.info("------------------------------------------");
        logger.info("[{}] [{}] [{}] [{}] [{} ms]", request.getRemoteAddr(), request.getMethod(), request.getRequestURL(), class_method, System.currentTimeMillis() - startTime);
        logger.info("-----------------REQUEST------------------");
        logger.info("request parameter: {}", mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request.getParameterMap())); 
        logger.info("request body: {}", getPayload(request));
        logger.info("-----------------RESPONSE-----------------");
        logger.info("RESPONSE: {}",this.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseBody)); 
        logger.info("------------------------------------------");

        // throw new ApiException(2000);

        return responseBody;
    }

    private String getPayload(HttpServletRequest request) {
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                try {
                    int length = Math.min(buf.length, 1024);
                    return new String(buf, 0, length, wrapper.getCharacterEncoding());
                } catch (UnsupportedEncodingException ex) {
                    return "[unknown]";
                }
            }
        }
        return "";
    }
}