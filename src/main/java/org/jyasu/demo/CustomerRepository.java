package org.jyasu.demo;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

  List<Customer> findByLastName(String lastName);

  Customer findById(long id);
  
  @Query(value = """
          select version() as test_version
          """)
  <T> Collection<T> getVersion(Class<T> type);
}