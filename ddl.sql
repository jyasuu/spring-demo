

-- public.account definition

-- Drop table

-- DROP TABLE public.account;

CREATE TABLE public.account (
	id varchar NOT NULL DEFAULT gen_random_uuid(),
	"name" varchar NOT NULL,
	"password" varchar NOT NULL,
	email varchar NOT NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	webhook_endpoint varchar NULL,
	status varchar NOT NULL DEFAULT 'Y'::character varying,
	CONSTRAINT account_pk PRIMARY KEY (id),
	CONSTRAINT account_un UNIQUE (name)
);




CREATE TABLE public.person (
	id varchar NOT NULL DEFAULT gen_random_uuid(),
	firstname varchar NOT NULL,
	lastname varchar NOT NULL
);
